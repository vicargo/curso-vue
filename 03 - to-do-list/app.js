const app = new Vue({
    el: '#app',
    data: {
        title : 'ToDo List con Vue.js',
        tasks: [],
        newTask: '',
    },
    created() {
        let dataLocal = JSON.parse(localStorage.getItem('local-tasks'))
        console.log(this.tasks)

        if(dataLocal === null) {
            this.tasks = [];
        }else {
            this.tasks = dataLocal
        }
    },
    methods : {
        addTask () {
            if(this.newTask != '') {
                this.tasks.push({
                    msg : this.newTask, completed : false
                })
            } 
            
            this.newTask = ''
            localStorage.setItem('local-tasks', JSON.stringify(this.tasks))
        },
        updateTask (index) {
            this.tasks[index].completed = true
            localStorage.setItem('local-tasks', JSON.stringify(this.tasks))
        },
        deleteTask (index) {
            this.tasks.splice(index, 1)
            localStorage.setItem('local-tasks', JSON.stringify(this.tasks))
        }
    },
    computed: {

    }
})