Vue.component('padre', {
    template: //html
    `
        <div class="p-5 bg-dark text-white">
            <h3>Componente Padre, su numero es: {{ numeroPadre }} y el nombre de su hijo es {{ nombreDeSuHijo }}</h3>
            <button class="btn btn-sm btn-primary mb-2" @click="numeroPadre++">+</button>
            <button class="btn btn-sm btn-danger mb-2" @click="numeroPadre--">-</button>
            <hijo :numero="numeroPadre" @nombreHijo="nombreDeSuHijo = $event"></hijo>

        </div>
    `,
    data(){
        return {
            numeroPadre: 0,
            nombreDeSuHijo: ''
        }
    }
})