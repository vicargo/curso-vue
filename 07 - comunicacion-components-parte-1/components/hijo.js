Vue.component('hijo', {
    template: //html
    `
        <div class="p-5 bg-success text-white">
            <h4>Componente hijo y el numero es: {{ numero }}</h4>
            <h4>Nombre: {{ nombre }}</h4>
        </div>
    `,
    props: ['numero'],
    data() {
        return {
            nombre: 'Nora'
        }
    },
    mounted() {
        this.$emit('nombreHijo', this.nombre);
    }
})