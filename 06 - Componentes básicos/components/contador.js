Vue.component('contador', {
    template: //html
    `
        <div>
            <p>valor: {{ value }}</p>
            <button class="btn btn-sm btn-primary" @click="value++">+</button>
            <button class="btn btn-sm btn-warning" v-if="value > 0" @click="value--">-</button>
        </div>
    `,
    data(){
        return {
            value: 0,
        }
    }
})