const app = new Vue( {
    el: '#app',
    data: {
        titulo: 'Frutas y colores',
        frutas: [
            { nombre: 'Manzana', cantidad: 2},
            { nombre: 'Pera', cantidad: 5},
            { nombre: 'Platano', cantidad: 0},
        ],
        nuevaFruta: '',
        nuevaCantidad: null,
        totalFrutas: 0
    },
    methods : {
        agregarFruta () {
            this.frutas.push( {
                nombre: this.nuevaFruta, cantidad: this.nuevaCantidad
            })
            this.nuevaFruta = '';
            this.nuevaCantidad = null;
        },
    },
    computed : {
        sumarFrutas() {
            this.totalFrutas = 0;

            for(fruta of this.frutas) {
                this.totalFrutas = this.totalFrutas + fruta.cantidad;
            }

            return this.totalFrutas;
        }
    }
})