const app = new Vue({
    el: '#app',
    data: {
        greeting : 'Lifecycle example'
    },
    beforeCreated() {
        console.log('Before created')
    },
    created() {
        // Al crear los métodos, observadores y eventos, pero aún no accede al DOM.
        // Aún no se puede acceder a 'el'
        console.log('Created')
    },
    beforeMount() {
        // Se ejecuta antes de insertar el DOM.
        console.log('Before mount')
    },
    mounted() {
        // Se ejecuta al insertar el DOM.
        console.log('mounted')
    },
    updated() {
        // Al realizar cambios
        console.log('updated')
    },
    beforeDestroy() {
        // Antes de destruir la instancia
        console.log('Before destroy')
    },
    destroyed() {
        // Se destruye la instancia
        console.log('destroyed')
    },
})